import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '@ngrx-getting-started/users';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent {
  public readonly pageTitle = 'Acme Product Management';

  public get isLoggedIn(): boolean {
    return this.authService.isLoggedIn;
  }

  public get userName(): string {
    if (this.authService.currentUser) return this.authService.currentUser.userName;

    return '';
  }

  public constructor(private router: Router, private authService: AuthService) {
    /**/
  }

  public logOut(): void {
    this.authService.logout();
    this.router.navigate(['/welcome']);
  }
}
