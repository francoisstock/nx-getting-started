import {
  Component,
  OnInit,
  Input,
  Output,
  OnChanges,
  EventEmitter,
  SimpleChanges
} from '@angular/core';

import { GenericValidator } from '../../shared/generic-validator';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NumberValidators } from '../../shared/number.validator';
import { ProductEntity } from '../../+state/products.models';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit, OnChanges {
  @Input() public product: ProductEntity;
  @Output() public readonly save = new EventEmitter<ProductEntity>();
  @Output() public readonly cancel = new EventEmitter();
  @Output() public readonly delete = new EventEmitter<ProductEntity>();
  @Output() public readonly showError = new EventEmitter<string>();

  // Use with the generic validation message class
  public displayMessage: { [key: string]: string } = {};
  private readonly validationMessages: {
    [key: string]: { [key: string]: string };
  };
  private readonly genericValidator: GenericValidator;

  public productForm: FormGroup;

  constructor(private readonly formBuilder: FormBuilder) {
    // Defines all of the validation messages for the form.
    // These could instead be retrieved from a file or database.
    this.validationMessages = {
      productName: {
        required: 'Product name is required.',
        minlength: 'Product name must be at least three characters.',
        maxlength: 'Product name cannot exceed 50 characters.'
      },
      productCode: {
        required: 'Product code is required.'
      },
      starRating: {
        range: 'Rate the product between 1 (lowest) and 5 (highest).'
      }
    };

    // Define an instance of the validator for use with this form,
    // passing in this form's set of validation messages.
    this.genericValidator = new GenericValidator(this.validationMessages);

    // Define the form group
    this.productForm = this.formBuilder.group({
      productName: [
        '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(50)]
      ],
      productCode: ['', Validators.required],
      starRating: [0, NumberValidators.range(1, 5)],
      description: ''
    });
  }

  public ngOnInit(): void {
    this.displayProduct(this.product);

    this.productForm.valueChanges.subscribe(_ => this.displayValidation());
  }

  public ngOnChanges(changes: SimpleChanges): void {
    const productChanges = changes['product'];
    const previous = productChanges.previousValue as ProductEntity;
    const current = productChanges.currentValue as ProductEntity;
    if (!previous || !current || previous === current) return;

    this.displayProduct(current);
  }

  displayProduct(product: ProductEntity | null): void {
    // Reset the form back to pristine
    this.productForm.reset();
    // Display the appropriate page title
    // if (this.product.id === 0) {
    //   this.pageTitle = 'Add Product';
    // } else {
    //   this.pageTitle = `Edit Product: ${this.product.productName}`;
    // }

    // Update the data on the form
    this.productForm.patchValue({
      productName: product.productName,
      productCode: product.productCode,
      starRating: product.starRating,
      description: product.description
    });
  }

  // Also validate on blur
  // Helpful if the user tabs through required fields
  public onBlur(): void {
    this.displayValidation();
  }

  private displayValidation(): void {
    this.displayMessage = this.genericValidator.processMessages(
      this.productForm
    );
  }

  public onSave(): void {
    if (!this.productForm.dirty) return;

    if (this.productForm.valid)
      // Copy over all of the original product properties
      // Then copy over the values from the form
      // This ensures values not on the form, such as the Id, are retained
      this.save.emit({ ...this.product, ...this.productForm.value });
    else this.onError('Please correct the validation errors.');
  }

  public onError(message: string): void {
    if (!!message) this.showError.emit(message);
  }

  public onDelete(): void {
    this.delete.emit(this.product);
  }

  public onCancel(): void {
    this.cancel.emit();
  }
}
