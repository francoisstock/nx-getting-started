import { Component, Input, Output, EventEmitter } from '@angular/core';

import { ProductEntity } from '../../+state/products.models';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent {
  public readonly pageTitle = 'Products';

  @Input() public readonly products: ProductEntity[];
  @Input() public readonly isCodeVisible: boolean;
  @Input() public readonly selectedProductId: number | null;
  @Input() public readonly errorMessage: string | null;

  @Output() public readonly checked = new EventEmitter<boolean>();
  @Output() public readonly initializeNewProduct = new EventEmitter();
  @Output() public readonly selected = new EventEmitter<ProductEntity>();

  public onChecked(value: boolean) {
    this.checked.emit(value);
  }

  public onInitializeNewProduct() {
    this.initializeNewProduct.emit();
  }

  public onSelected(product: ProductEntity): void {
    this.selected.emit(product);
  }
}
