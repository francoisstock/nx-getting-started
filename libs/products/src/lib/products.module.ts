import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { ProductListComponent, ProductFormComponent } from './components';
import { ProductShellComponent, ProductEditComponent } from './containers';
import { ROUTES } from './products.routes';
import * as fromProducts from './+state/products.reducer';
import { ProductsEffects } from './+state/products.effects';
import { ProductsFacade } from './+state/products.facade';

@NgModule({
  imports: [
    CommonModule,

    FormsModule,
    ReactiveFormsModule,

    RouterModule.forChild(ROUTES),
    StoreModule.forFeature(
      fromProducts.PRODUCTS_FEATURE_KEY,
      fromProducts.reducer
    ),
    EffectsModule.forFeature([ProductsEffects])
  ],
  declarations: [
    ProductEditComponent,
    ProductListComponent,
    ProductShellComponent,
    ProductFormComponent
  ],
  providers: [ProductsFacade]
})
export class ProductsModule {}
