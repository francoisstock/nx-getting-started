import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ProductsFacade } from '../../+state/products.facade';

import * as ProductsActions from '../../+state/products.actions';
import { ProductEntity } from '../../+state/products.models';

@Component({
  templateUrl: './product-shell.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductShellComponent implements OnInit {
  public products$ = this.facade.allProducts$;
  public isCodeVisible$ = this.facade.isCodeVisible$;
  public selectedProductId$ = this.facade.selectedProductId$;
  public errorMessage$ = this.facade.error$;

  public constructor(private readonly facade: ProductsFacade) { }

  public ngOnInit(): void {
    this.facade.dispatch(ProductsActions.loadProducts());
  }

  public onChecked(value: boolean): void {
    this.facade.dispatch(
      ProductsActions.toggleCodeVisibllity({ isVisible: value })
    );
  }

  public onNewInitialize(): void {
    this.facade.dispatch(ProductsActions.initializeNewProduct());
  }

  public onProductSelected(product: ProductEntity): void {
    this.facade.dispatch(ProductsActions.setCurrentProduct({ product }));
  }
}
