import { Component, ChangeDetectionStrategy } from '@angular/core';

import * as ProductsAction from '../../+state/products.actions';
import { ProductEntity } from '../../+state/products.models';
import { ProductsFacade } from '../../+state/products.facade';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductEditComponent {
  public readonly product$ = this.facade.selectedProduct$;
  public pageTitle = 'Product Edit';
  public errorMessage = '';

  constructor(private readonly facade: ProductsFacade) {
    /**/
  }

  public onSave(product: ProductEntity): void {
    if (product.id === 0)
      this.facade.dispatch(ProductsAction.createNewProduct({ product }));
    else this.facade.dispatch(ProductsAction.updateProduct({ product }));
  }

  public onDelete(product: ProductEntity): void {
    if (this.canBeDeleted(product)) {
      if (confirm(`Really delete the product: ${product.productName}?`))
        this.facade.dispatch(ProductsAction.deleteProduct({ product }));
    } else this.onCancel(); // No need to delete, it was never saved
  }

  private canBeDeleted(product: ProductEntity): boolean {
    return product && !!product.id;
  }

  public onCancel(): void {
    if (confirm('Really want loose changes?'))
      this.facade.dispatch(ProductsAction.clearCurrentProduct());
  }

  public onError(message: string): void {
    this.errorMessage = message;
  }
}
