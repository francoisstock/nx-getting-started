import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as ProductsActions from './products.actions';
import { ProductEntity } from './products.models';

export const PRODUCTS_FEATURE_KEY = 'products';

export interface State extends EntityState<ProductEntity> {
  selectedId?: number | null;
  isLoaded: boolean; // has the Products list been loaded
  error?: string | null; // last none error (if any)

  isCodeVisible: boolean;
}

export interface ProductsPartialState {
  readonly [PRODUCTS_FEATURE_KEY]: State;
}

export const productsAdapter: EntityAdapter<ProductEntity> = createEntityAdapter<
  ProductEntity
>();

export const initialState: State = productsAdapter.getInitialState({
  // set initial required properties
  isLoaded: false,
  isCodeVisible: true
});

const productsReducer = createReducer(
  initialState,
  on(ProductsActions.loadProducts, state => ({
    ...state,
    isLoaded: false,
    error: null
  })),
  on(ProductsActions.loadProductsSuccess, (state, { products }) =>
    productsAdapter.setAll(products, { ...state, isLoaded: true })
  ),
  on(ProductsActions.loadProductsFailure, (state, { error }) => ({
    ...state,
    error
  })),
  on(ProductsActions.toggleCodeVisibllity, (state, { isVisible }) => ({
    ...state,
    isCodeVisible: isVisible
  })),
  on(ProductsActions.createNewProduct, (state, { product }) =>
    productsAdapter.addOne(product, { ...state, selectedId: product.id })
  ),
  on(ProductsActions.updateProduct, (state, { product }) =>
    productsAdapter.setOne(product, state)
  ),
  on(ProductsActions.deleteProduct, (state, { product }) =>
    productsAdapter.removeOne(product.id, {...state, selectedId: null})
  ),
  on(ProductsActions.setCurrentProduct, (state, { product }) => ({
    ...state,
    selectedId: product.id
  })),
  on(ProductsActions.clearCurrentProduct, state => ({
    ...state,
    selectedId: null
  })),
  on(ProductsActions.initializeNewProduct, state => ({
    ...state,
    selectedId: 0
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return productsReducer(state, action);
}
