import { Injectable } from '@angular/core';

import { select, Store, Action } from '@ngrx/store';

import * as fromProducts from './products.reducer';
import * as ProductsSelectors from './products.selectors';

@Injectable()
export class ProductsFacade {
  isLoaded$ = this.store.pipe(select(ProductsSelectors.getProductsLoaded));
  allProducts$ = this.store.pipe(select(ProductsSelectors.getAllProducts));
  selectedProduct$ = this.store.pipe(select(ProductsSelectors.getSelected));
  selectedProductId$ = this.store.pipe(select(ProductsSelectors.getSelectedId));
  isCodeVisible$ = this.store.pipe(select(ProductsSelectors.isCodeVisible))
  error$ = this.store.pipe(select(ProductsSelectors.getProductsError));

  constructor(private store: Store<fromProducts.ProductsPartialState>) {}

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}
