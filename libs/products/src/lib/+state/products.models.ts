/**
 * Interface for the 'Products' data
 */
export interface ProductEntity {
  id: number | null;
  productName: string;
  productCode: string;
  description: string;
  starRating: number;
}
