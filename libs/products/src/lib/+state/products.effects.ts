import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType, act } from '@ngrx/effects';
import { fetch } from '@nrwl/angular';

import { mergeMap, catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';

import * as ProductsActions from './products.actions';
import { ProductService } from '../product.service';

@Injectable()
export class ProductsEffects {
  public readonly loadProducts$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ProductsActions.loadProducts),
      mergeMap(_ =>
        this.service.getProducts().pipe(
          map(products => ProductsActions.loadProductsSuccess({ products })),
          catchError(error =>
            of(ProductsActions.loadProductsFailure({ error }))
          )
        )
      )
    );
  });


  public readonly deleteProduct$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ProductsActions.deleteProduct),
      map(action => action.product),
      mergeMap(product =>
        this.service.deleteProduct(product.id).pipe(
          map(_ =>
            ProductsActions.deleteProductSuccess()
            ),
            catchError(error =>
              of(ProductsActions.deleteProductFailure({ error }))
              )
        )
        )
        );
      });

      public readonly createProduct$ = createEffect(() => {
        return this.actions$.pipe(
          ofType(ProductsActions.createNewProduct),
          map(action => action.product),
          mergeMap(product =>
            this.service.createProduct(product).pipe(
              map(createdProduct =>
                ProductsActions.createNewProductSuccess({ product: createdProduct })
              ),
              catchError(error =>
                of(ProductsActions.createNewProductFailure({ error }))
              )
            )
          )
        );
      });

      public readonly updateProduct$ = createEffect(() => {
        return this.actions$.pipe(
      ofType(ProductsActions.updateProduct),
      map(action => action.product),
      mergeMap(product =>
        this.service.updateProduct(product).pipe(
          map(updatedProduct =>
            ProductsActions.updateProductSuccess({ product: updatedProduct })
          ),
          catchError(error =>
            of(ProductsActions.updateProductFailure({ error }))
          )
        )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly service: ProductService
  ) {}
}
