import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  PRODUCTS_FEATURE_KEY,
  State,
  ProductsPartialState,
  productsAdapter
} from './products.reducer';

// Lookup the 'Products' feature state managed by NgRx
export const getProductsState = createFeatureSelector<
  ProductsPartialState,
  State
>(PRODUCTS_FEATURE_KEY);

const { selectAll, selectEntities } = productsAdapter.getSelectors();

export const getProductsLoaded = createSelector(
  getProductsState,
  (state: State) => state.isLoaded
);

export const getProductsError = createSelector(
  getProductsState,
  (state: State) => state.error
);

export const getAllProducts = createSelector(getProductsState, (state: State) =>
  selectAll(state)
);

export const getProductsEntities = createSelector(
  getProductsState,
  (state: State) => selectEntities(state)
);

export const getSelectedId = createSelector(
  getProductsState,
  (state: State) => state.selectedId
);

export const getSelected = createSelector(
  getProductsEntities,
  getSelectedId,
  (entities, selectedId) => {
    if(selectedId === 0) return {
      id: 0,
      productName: '',
      productCode: 'New',
      description: '',
      starRating: 0
    };
    else return selectedId && entities[selectedId];
  }
);

export const isCodeVisible = createSelector(
  getProductsState,
  (state: State) => state.isCodeVisible
);
