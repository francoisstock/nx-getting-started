import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as UsersActions from './users.actions';
import { UsersEntity } from './users.models';

export const USERS_FEATURE_KEY = 'users';

export interface State extends EntityState<UsersEntity> {
  selectedId?: string | number; // which Users record has been selected
  isLoaded: boolean; // has the Users list been loaded
  isUseNameMasked: boolean;
  error?: string | null; // last none error (if any)
}

export interface UsersPartialState {
  readonly [USERS_FEATURE_KEY]: State;
}

export const usersAdapter: EntityAdapter<UsersEntity> = createEntityAdapter<
  UsersEntity
>();

export const initialState: State = usersAdapter.getInitialState({
  // set initial required properties
  isLoaded: false,
  isUseNameMasked: false
});

const usersReducer = createReducer(
  initialState,
  on(UsersActions.loadUsers, state => ({
    ...state,
    isLoaded: false,
    error: null
  })),
  on(UsersActions.loadUsersSuccess, (state, { users }) =>
    usersAdapter.setAll(users, { ...state, isLoaded: true })
  ),
  on(UsersActions.loadUsersFailure, (state, { error }) => ({ ...state, error })),
  on(UsersActions.toggleUserNameVisibility, (state, { isMasked }) => ({
    ...state,
    isUseNameMasked: isMasked
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return usersReducer(state, action);
}
