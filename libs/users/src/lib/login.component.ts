import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';
import * as UsersActions from './+state/users.actions'
import { UsersFacade } from './+state/users.facade';

@Component({
  selector: 'ngrx-getting-started-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public readonly pageTitle = 'Log In';
  public readonly isUserNameMasked$ = this.facade.isUserNameMasked$;

  public errorMessage: string;


  public constructor(
    private readonly authService: AuthService,
    private readonly facade: UsersFacade,
    private readonly router: Router
  ) { /**/ }

  public ngOnInit(): void {}

  public cancel(): void {
    this.router.navigate(['welcome']);
  }

  public checkChanged(isMasked: boolean): void {
    this.facade.dispatch(UsersActions.toggleUserNameVisibility({ isMasked }));
  }

  public login(loginForm: NgForm): void {
    if (loginForm && loginForm.valid) {
      const userName = loginForm.form.value.userName;
      const password = loginForm.form.value.password;
      this.authService.login(userName, password);

      if (this.authService.redirectUrl) {
        this.router.navigateByUrl(this.authService.redirectUrl);
      } else {
        this.router.navigate(['/products']);
      }
    } else {
      this.errorMessage = 'Please enter a user name and password.';
    }
  }
}
