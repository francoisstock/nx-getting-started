import { Injectable } from '@angular/core';

import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public redirectUrl: string;
  #currentUser: User | null;

  public get isLoggedIn(): boolean {
    return !!this.#currentUser;
  }

  public get currentUser(): User | null {
    return this.currentUser;
  }
  
  public login(userName: string, password: string): void {
    // Code here would log into a back end service
    // and return user information
    // This is just hard-coded here.
    this.#currentUser = {
      id: 2,
      userName,
      isAdmin: false
    };
  }
  
    public logout(): void {
      this.#currentUser = null;
    }
}
