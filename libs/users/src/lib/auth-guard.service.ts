import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router'

import { AuthService } from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) { /**/ }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.checkLoggedIn(state.url);
  }

  private checkLoggedIn(url: string): boolean {
    if(this.authService.isLoggedIn) return true;

    // Retain the attempted Url for redirection
    this.authService.redirectUrl = url;
    this.router.navigate(['/login']);

    return false;
  }
}
